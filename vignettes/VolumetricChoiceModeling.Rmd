---
title: "Volumetric Choice Model workflow with demo data"
output: 
  rmarkdown::html_document:
    theme: spacelab
    highlight: pygments
    toc: true
    toc_float: true
    toc_depth: 3
    number_sections: no
    fig_caption: yes
vignette: >
  %\VignetteIndexEntry{Volumetric Choice Model workflow with demo data}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---


```{r setup, include = FALSE}
  library(echoice)
  library(pacman)

  pacman::p_load(purrr,tidyr,ggplot2,kableExtra)
  
  knitr::opts_chunk$set(fig.align = "center",
                        fig.height = 3.5,
                        warning = FALSE,
                        error = FALSE,
                        message = FALSE)
```



# Introduction

The MCMC algorithm for the volumetric demand model is implemented in `rVDev`. 
The model can be applied to data from volumetric conjoint studies, or from unit-level demand data, such as household panel data.
This vignette provides exposition of the model, input data structure, and an example, including model estimation and evaluation.


# Model

We assume that subjects maximize their utility from consuming inside goods $x$ and the outside good $z$

$$u\left( {{\bf{x}},z} \right) = \sum\limits_k {\frac{{{\psi _k}}}{\gamma }} \ln \left( {\gamma {x_k} + 1} \right) + \ln (z)$$
subject to a budget constraint
$$\sum\limits_k^{} {{p_k}} {x_k} + z = E$$

where $\psi_k$ is the baseline utlity of the $k$-th good, and $\gamma$ is a parameter that governs the rate of satiation of inside goods. It is assumed that the rate of satiation $\gamma$ is the same for all inside goods.

In this implementation, we assume that
$$\psi_j = \exp[a_j'\beta + \varepsilon_j]$$
where
$$\varepsilon \sim \text{Type I EV}(0,\sigma)$$


The marginal utility for the inside and outside goods is:

$$
\begin{array}{*{20}{l}}
{{u_j}}&{ = \frac{{\partial u\left( {{\bf{x}},z} \right)}}{{\partial {x_j}}} = \frac{{{\psi _j}}}{{\gamma {x_j} + 1}}{\rm{  }}}\\
{{u_z}}&{ = \frac{{\partial u\left( {{\bf{x}},z} \right)}}{{\partial z}} = \frac{1}{z}}
\end{array}
$$


Solving for $\varepsilon_j$ leads to the following expression for the KT conditions:

$$\begin{array}{*{20}{l}}
{}&{{\varepsilon _j} = {g_j}\quad {\rm{if}}\quad {x_j} > 0}\\
{}&{{\varepsilon _j} < {g_j}\quad {\rm{if}}\quad {x_j} = 0}
\end{array}$$


where
$${g_j} =  - {{\bf{a}}_{j'}}\beta  + \ln (\gamma {x_j} + 1) + \ln \left( {\frac{{{p_j}}}{{E - {{\bf{p}}^\prime }{\bf{x}}}}} \right)$$

We are able to obtain a closed-form expression for the probability that $R$ of $N$ goods are chosen.  

$$ x_1,x_2,\dots,x_R > 0, \qquad x_{R+1}, x_{R+2}, \dots, x_N = 0. $$

The error scale $(\sigma$) is identified in this model because price enters the specification without a separate price coefficient
The likelihood $\ell(\theta)$ of the model parameters is proportional to the probability of observing $n_1$ chosen goods and $n_2$ goods with zero demand 
The contribution to the likelihood of the chosen goods is in the form of a probability density  while the goods not chosen contribute as a probability mass.


$$\begin{array}{l}
\ell (\theta ) \propto p({x_{{n_1}}} > 0,{x_{{n_2}}} = 0|\theta )\\
 = |{J_R}|\int_{ - \infty }^{{g_N}}  \cdots  \int_{ - \infty }^{{g_{R + 1}}} f ({g_1}, \ldots ,{g_R},{\varepsilon _{R + 1}}, \ldots ,{\varepsilon _N})d{\varepsilon _{R + 1}}, \ldots ,d{\varepsilon _N}\\
 = |{J_R}|\left\{ {\prod\limits_{j = 1}^R {\frac{{\exp ( - {g_j}/\sigma )}}{\sigma }} \exp \left( { - {e^{ - {g_j}/\sigma }}} \right)} \right\}\left\{ {\prod\limits_{i = R + 1}^N {\exp } \left( { - {e^{ - {g_i}/\sigma }}} \right)} \right\}\\
 = |{J_R}|\left\{ {\prod\limits_{j = 1}^R {\frac{{\exp ( - {g_j}/\sigma )}}{\sigma }} } \right\}\exp \left\{ { - \sum\limits_{i = 1}^N {\exp } ( - {g_i}/\sigma )} \right\}
\end{array}$$


where $|J_{R}|$ is the Jacobian of the transformation from random-utility error ($\varepsilon$) to the likelihood of the observed data. In this model, the Jacobian is:
$$\left| {{J_R}} \right| = \prod\limits_{k = 1}^R {\left( {\frac{\gamma }{{\gamma {x_k} + 1}}} \right)} \left\{ {\sum\limits_{k = 1}^R {\frac{{\gamma {x_k} + 1}}{\gamma }} \cdot\frac{{{p_k}}}{{E - {{\bf{p}}^\prime }{\bf{x}}}} + 1} \right\}$$


The likelihood function is implemented in the R function `vdes_ll`.




The 'lower level' of the hierarchical model applies the direct utility model to a specific respondent's choice data, and the 'upper level' of the model incorporates heterogeneity in respondent coefficients.    

Respondent heterogeneity can be incorporated into conjoint analysis using a variety of random-effect models. We use a Normal model for heterogeneity in `rVDMevs`.  

Denoting all individual-level parameters $\theta_h$ for respondent $h$ we have:
$$
\theta_h \sim \text{Normal}(\bar\theta, \Sigma) \label{eq:hetero}
$$
where $$\theta_h =\left\{ {\beta_h', \gamma_h, E_h,\sigma_h} \right\}$$.
It is possible to add covariates ($z$) to the mean of the heterogeneity distribution as in a regression model:
$$
\theta_h \sim  \text{Normal}(\Gamma z_h, \Sigma)
$$
The parameters $\bar\theta$, $\Gamma$ and $\Sigma$ are referred to as hyper-parameters because they describe the distribution of other parameters.  
Covariates in the above expression might include demographic variables or other variables collected as part of the conjoint survey, e.g., variables describing reasons to purchase.  
`rVDev` implements a multivariate Regression prior. If no covariates are provided, $z$ simply contains a vector of `1`s, and $\Gamma$ is equivalent to $\bar \theta$ in the MVN heterogeneity model.



# Priors

`rVDev`'s second argument is Prior. It contains a list of (Gammabar, AGamma, nu, V)


Natural conjugate priors are specified:

$$ \theta \sim MVN({\Gamma}z_h, \Sigma) $$
$$\Gamma \sim (\bar \Gamma, A_{\Gamma}^{-1})$$

$$ \Sigma \sim IW(\nu, V) $$

This specification of priors assumes that $\Gamma$ is independent of $\Sigma$ and that, conditional on the hyperparameters, the $\theta_i$'s are a priori independent. 




# Example

Here we demonstrate the implementation using the `ICdata` ice cream data available within the package. The dataset contains volumetric choice data for 601 respondents who evaluated packaged ice cream.
The data are stored in a lists-of-lists format with one list per respondent, and each respondent's list having three elements: a matrix of volumetric choices (`X`), a matrix of prices with matching dimensions (`P`) and a matrix of covariates (`A`), which usually contains the stacked design matrices matching the products subjects were able to choose from. 

There are 601 respondents in this dataset:
```{r}
  data(ICdata)
  length(ICdata[[1]])
```

Respondent 1's responses are stored in a list containing X, A, P:
```{r}
  str(ICdata[[1]][[1]])
```

Full list of attributes and levels:
```{r}
list(Brand =c("Breyers",colnames(ICdata[[1]][[1]]$A[,2:7])),
     Flavor=c("Vanilla",colnames(ICdata[[1]][[1]]$A[,8:16])),
     Size  =c('size4',  colnames(ICdata[[1]][[1]]$A[,17:18])))
```


## Preparing data

The estimation function `rVDev` can use both list-of-list or long data format. 
For illustration purposes, we convert the list-of-lists to the 'long' data format using `vd_list2stackedplus`:
```{r}
  ICdata_voldata_long = vd_list2stackedplus(ICdata$voldata) %>% data.frame
  head(ICdata_voldata_long)
```
`vd_list2stackedplus` returns the following columns:
* ID (respondent identifier)
* task (task number)
* alternative (alternative number within task)
* x (quantity purchased)
* p (price)
* sxp (total expenditure within task)
* n (number of alternatives in task)
* attributes defining the choice alternatives


## A quick look at purchase quantities

Using the long format, it is easy to create graphical summaries using ggplot2.
For instance, we can show the distribution of primary demand (total purchase quantities) per task across the 601 respondents in the dataset.

```{r}
  ICdata_voldata_long %>% 
  group_by(ID, task) %>% summarize(xtot=sum(x)) %>% 
  group_by(ID) %>% summarize(xtotmean=mean(xtot)) %>% 
  ggplot(aes(x=xtotmean)) + geom_histogram()
```

## Data cleaning

Based on the distribution, we may want to exclude respondents with extreme purchase quantities. Here, we eliminate respondents who buy more than 15 tubs of ice cream on average.

```{r}
ICdata_filter = ICdata_voldata_long %>% 
  group_by(ID, task) %>% summarize(xtot=sum(x)) %>% 
  group_by(ID) %>% summarize(xtotmean=mean(xtot)) %>% 
  mutate(filter=xtotmean>=15)

ICdata_voldata_long_filtered = ICdata_voldata_long %>% 
  left_join(ICdata_filter) %>% 
  filter(filter==FALSE) %>% select(-filter,-xtotmean)
```


## Hold out

For model validation, we want to split the data into into train and test data. 
For each respondent, we randomly pick 2 choice tasks for validation, while using the remaining tasks for model estimation.
Here, we add a column which identifies train and test data:

```{r}
  set.seed(123)
    ICdata_split = ICdata_voldata_long_filtered %>% select(ID,task) %>% 
      distinct() %>% 
      group_by(ID) %>% add_count() %>% 
      mutate(data=ifelse(task %in% sample(1:12,2),'test','train'))
  rm(list=".Random.seed", envir=globalenv())

  ICdata_voldata_long = ICdata_voldata_long_filtered %>% left_join(ICdata_split)
  head(ICdata_voldata_long)
```


## Model Estimation

Estimate model on training data only (standard model is implemented in `rVDev`):

```{r, eval=TRUE, echo = FALSE}
  load('startval.rda')
```

<!-- code we want to run -- this is what is shown in the document to the reader -->
```{r eval=TRUE, include=TRUE}
  out_IC = rVDev(Data = list(voldatalong=ICdata_voldata_long %>% 
                               filter(data=='train') %>% select(-data) ),
                 Mcmc = list(R=2000, 
                             thetastart=startval, tunestart=1, tunelength=1, keep=1))
```
<!-- code actually run -- not shown to the reader, done to suppress output -->
```{r, eval=FALSE, echo = FALSE}
  load('out_IC.rdata')
```



We can also run `rVDev` using a list-of-lists specification. The example below estimates the model based on all tasks provided in the ice cream data set in a list format. Here, the data lists includes an elemen called `voldata` instead of `voldatalong`.
```{r eval=FALSE, include=TRUE}
  out_IC = rVDev(Data = list(voldata=ICdata$voldata),
                 Mcmc = list(R=2000))
```


Output from `rVDMevs` is a list with the following

```{r}
out_IC%>%str
```

* `thetaDraw`: An array of dimension `Nunits` X `p` X `Ndraws/keep` where `p` is he number of model parameters
* `GammaDraw`: A matrix of dimension `Ndraws/keep` X `p`*`m`, where `m` is the number of upper-level covariates
* `SigmaThetaDraw`: An array of dimension `p` X `p` X `Ndraws/keep`
* `logLike`: A vector of length `Ndraws/keep` containing the log-likelihood values




## Diagnostics

It is easy to use ggplot2 to produce traceplots of the 'upper level' parameters.

```{r}
  out_IC %>% pluck('GammaDraw') %>% 
    data.frame %>% rownames_to_column(var = 'MCMCiter') %>% 
    gather(Variable,Value,-MCMCiter) %>% 
    mutate(MCMCiter=as.numeric(MCMCiter), 
           Variable=factor(Variable,unique(Variable))) %>% 
    ggplot(aes(x=MCMCiter,y=Value, color=Variable)) + 
      geom_line() + theme_minimal() + guides(color=FALSE)
```



```{r}
  out_IC %>% pluck('GammaDraw') %>% `[`(-(1:1000),) %>%
    data.frame %>% rownames_to_column(var = 'MCMCiter') %>% 
    gather(Variable,Value,-MCMCiter) %>% 
    mutate(MCMCiter=as.numeric(MCMCiter), 
           Variable=factor(Variable,unique(Variable))) %>% 
    ggplot(aes(x=MCMCiter,y=Value, color=Variable)) + 
      geom_line() + theme_minimal()  + guides(color=FALSE)
```



## Parameter Estimates

Many users would first look at summaries of GammaDraw. If no covariates are used, they show average 'part-worths', budget, satiation:

```{r}
  out_IC %>% pluck('GammaDraw') %>% (function(x)x[ceiling(nrow(x)/2):nrow(x),]) %>% 
    data.frame %>% rownames_to_column(var = 'MCMCiter') %>% 
    gather(Variable,Value,-MCMCiter) %>% 
    mutate(MCMCiter=as.numeric(MCMCiter), 
           Variable=factor(Variable,unique(Variable))) %>% 
    ggplot(aes(x=Variable,y=Value)) + geom_boxplot() +coord_flip()
```


Using tidy syntax it is easy to highlight 'significant' parameters, i.e. parameters whose 90% or 95% interval of the posterior distribution does not include 0:

We can also show results in a table format:

```{r}
  nonzero95=
    out_IC %>% pluck('GammaDraw') %>% 
    (function(x)x[ceiling(nrow(x)/2):nrow(x),]) %>% data.frame %>% 
    rownames_to_column(var = 'MCMCiter') %>% 
      gather(Variable,Value,-MCMCiter) %>% 
      mutate(MCMCiter=as.numeric(MCMCiter), 
             Variable=factor(Variable,unique(Variable)))  %>% 
      group_by(Variable) %>% 
  summarise(Nonzero=prod(quantile(Value,probs = c(.025,.975)))>0)

  parest= out_IC %>% pluck('GammaDraw')  %>% 
  (function(x)x[ceiling(nrow(x)/2):nrow(x),])  %>% data.frame %>% 
    rownames_to_column(var = 'MCMCiter') %>% 
    gather(Variable,Value,-MCMCiter) %>% 
    mutate(MCMCiter=as.numeric(MCMCiter), 
           Variable=factor(Variable,unique(Variable))) %>% 
    group_by(Variable) %>% 
    summarise(mean=mean(Value),sd=sd(Value),
              centile5=quantile(Value,probs=.05),
              centile95=quantile(Value,probs=.95)) %>%
    left_join(nonzero95)
```

and even output it in a html format:

```{r, results='asis'}
   parest %>% 
    kable('html', longtable=F,booktabs=T, 
          caption='Estimates', digits=2, escape=F, linesep="") %>%
    kable_styling(latex_options = c("repeat_header"), position = "center")
```




## Model evaluation

Here, we evaluate in-sample and out-of-sample fit of the estimated demand model.

### Out-of-sample fit

A key criterion is the ability of the model to accurate predict 'out of sample', i.e. predict demand for the test data. 
The `VDes_demand` quickly generates draws from the posterior distrubtion of demand. Here, we predict demand for the 'test' portion of our data set, i.e. for 2 hold out tasks per individual household:

```{r}
  o1_oosample_demand = 
    out_IC$thetaDraw[,,1000:2000] %>%
      VDes_demand(ICdata_voldata_long%>% 
                    filter(data=='test') %>% select(-data), neps=1)
```


It is easy to summarize posterior mean estimates of demand:
```{r}
   o1_oosample_demandPM = 
    o1_oosample_demand %>% group_by(ID,task,alternative) %>% summarise(xhat=mean(x))
```

Finally, we evaluate hold-out predictions in terms of Mean Squared Error (MSE), Mean Absolute Error (MAE) and Relative Absolute Error (RAE), which are defined as follows:

\[{\rm{MSE}}({\bf{x}},\widehat {\bf{x}})\quad  = \frac{{\sum\nolimits_{i = 1}^n {{{\left( {{{\widehat {\bf{x}}}_i} - {{\bf{x}}_i}} \right)}^2}} }}{n}\]
\[{\rm{MAE}}({\bf{x}},\widehat {\bf{x}})\quad  = \frac{{\sum\nolimits_{i = 1}^n {\left| {{{\widehat {\bf{x}}}_i} - {{\bf{x}}_i}} \right|} }}{n}\]
\[{\rm{RAE}}({\bf{x}},\widehat {\bf{x}})\quad  = \frac{{\sum\nolimits_{i = 1}^n {\left| {{{\widehat {\bf{x}}}_i} - {{\bf{x}}_i}} \right|} }}{{\sum\nolimits_{i = 1}^n {\left| {\overline {\bf{x}}  - {{\bf{x}}_i}} \right|} }}\]

```{r}
mse=function(xhat,x)mean((x-xhat)^2)
mae=function(xhat,x)mean(abs(x-xhat))
rae=function(xhat,x)mean(abs(xhat-x),na.rm=T)/mean(abs(mean(x,na.rm=T)-x),na.rm=T)

 o1_oosample_demandPM %>% 
   left_join(ICdata_voldata_long %>% 
               filter(data=='test') %>% select(ID,task,alternative,x)) %>% 
   transmute(alternative=alternative, 
             mse=mse(xhat,x), 
             mae=mae(xhat,x), 
             rae=rae(xhat,x)) %>%  
   ungroup() %>% 
   summarize(mse=mean(mse),mae=mean(mae),rae=mean(rae[is.finite(rae)]))
```


### In-sample fit

Simple Marginal Likelihood approximation:
```{r}
  pacman::p_load(bayesm)
  out_IC %>% pluck('logLike') %>% tail(1000) %>% bayesm::logMargDenNR()
```


We can also apply classic statistical fit criteria (MSE, MAE, RAE) to in-sample predictions:

```{r}
  o1_insample_demand = 
    out_IC$thetaDraw[,,1000:2000] %>%
      VDes_demand(ICdata_voldata_long%>% 
                    filter(data=='train') %>% select(-data), neps=1)
 
  o1_insample_demandPM = 
    o1_insample_demand %>% group_by(ID,task,alternative) %>% summarise(xhat=mean(x))
  
   o1_insample_demandPM %>% 
   left_join(ICdata_voldata_long %>% 
               filter(data=='train') %>% select(ID,task,alternative,x)) %>% 
   transmute(alternative=alternative, 
             mse=mse(xhat,x), 
             mae=mae(xhat,x), 
             rae=rae(xhat,x)) %>%  
   ungroup() %>% 
   summarize(mse=mean(mse),mae=mean(mae),rae=mean(rae[is.finite(rae)]))
```


## Market Simulation

For market simulations, we can create several scenarios. For illustration, we only generate demand predictions for one scenario, based off the first choice task of the first respondent. 

```{r}
  Market_Offerings = ICdata[[1]][[1]]$A[1:12,]
  Market_Prices = ICdata[[1]][[1]]$P[1,,drop=F]
  
  scenarios=list()
  scenarios[[1]] = data.frame(Market_Offerings, pr = drop(Market_Prices)) %>% data.matrix
```


Convert to long format. We only need ID, task, alternative, p and attributes. It is possible to stack up many different scenarios as different tasks.
```{r}
  vd_msimlong=function(qq,ID=1, task=1){
  nalt=nrow(qq)
  return(data.frame(ID=ID,task=task,alternative=1:nalt,p=qq[,ncol(qq)],n=nalt,qq[,-ncol(qq)]))
  }
  
  msim_vlong=vd_msimlong(scenarios[[1]])
  head(msim_vlong)
```



Market simulations should not be based off 'posterior draws'. Instead, we can simulate decision makers based on the 'upper level model'. We simulate a market with 100 decisions makers using the `vdev_generalize` function:

```{r}
  o1_theta_market_sim100 = 
    out_IC %>% vdev_generalize(nunits = 100)
```




We need to make the design matrix information match, requiring to replicate the design matrix information 100 times.
```{r}
   msim_vlong_100=msim_vlong[rep(1:nrow(msim_vlong),100),]
   msim_vlong_100$ID=rep(1:100,each=nrow(msim_vlong))
```


Then it is easy to generate demand predictions using `VDes_demand`:
```{r}
  o1_market100_sim_demand = 
    o1_theta_market_sim100 %>% 
      VDes_demand(voldatalong = msim_vlong_100, neps = 10 )
```



We can create at marketplace totals, and obtain posterior mean estimates of demand:
```{r}
  o1_market100_sim_demand_markettotals = 
    o1_market100_sim_demand %>% 
      group_by(task,alternative,draw) %>% summarise(X=sum(x))

 o1_market100_sim_demand_markettotals %>% 
   group_by(alternative) %>% summarize(x=mean(X))
```

We can also explore the posterior distribution of marketplace demand:
```{r}
 o1_market100_sim_demand_markettotals %>% ungroup %>% 
  mutate(alternative=factor(alternative)) %>% 
  ggplot(aes(x=alternative,y=X))+geom_boxplot()
```

We can also create a simple barchart showing marketplace demand:
```{r}
 o1_market100_sim_demand_markettotals %>% ungroup %>% 
  mutate(alternative=factor(alternative)) %>% 
  ggplot(aes(x=alternative,y=X))+
    geom_bar(stat = "summary", fun.y = "mean")
```


