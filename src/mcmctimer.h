#include <Rcpp.h>
using namespace Rcpp;

//borrowed from bayesm, enhanced

time_t itime;
time_t itime2;

void startMcmcTimer() {
  itime = time(NULL);
  Rcout << " MCMC in progress \n";
}

void endMcmcTimer() {
  time_t ctime = time(NULL);
  REprintf("\n MCMC complete\n");
  Rprintf(" Total Time Elapsed: %.2f minutes\n", difftime(ctime, itime) / 60.0);     
  itime = 0;
}

void infoMcmcTimer(int rep, int R) {
  time_t ctime = time(NULL);    

  //time to end
  double timetoend = difftime(ctime, itime) / 60.0 * (R - rep - 1) / (rep+1);
  
  //percent done, typecast
  double perc1=(double)rep/(double)R;
  
  //round percent done
  int perc = floor(perc1*100+0.5);
  
  //overwrite output with current status
  REprintf("\r");
  REprintf("Iteration: %i of %i (%i percent), ETA: %.2f min.", rep, R, perc, timetoend);
  
}


void infoMcmcTimerRRLL(int rep, int R, double RejectionRate, double LogLL) {
  time_t ctime = time(NULL);    

  //time to end
  double timetoend = difftime(ctime, itime) / 60.0 * (R - rep - 1) / (rep+1);
  
  //percent done, typecast
  double perc1=(double)rep/(double)R;
  
  //round percent done
  int perc = floor(perc1*100+0.5);
  
  //round RR
  int RR  = floor(RejectionRate*100+0.5);
  
  //overwrite output with current status
  REprintf("\r");
  REprintf("Iteration: %i of %i (%i percent), ETA: %.2f min., RR: %i, LogLL: %.1f", rep, R, perc, timetoend, RR, LogLL);
  
}

void startTimer() {
  itime2 = time(NULL);
  Rcout << " Computation in progress \n";
}

void infoTimer(int rep, int R) {
  time_t ctime = time(NULL);    
  
  //time to end
  double timetoend = difftime(ctime, itime2) / 60.0 * (R - rep - 1) / (rep+1);
  
  //percent done, typecast
  double perc1=(double)rep/(double)R;
  
  //round percent done
  int perc = floor(perc1*100+0.5);
  
  //overwrite output with current status
  REprintf("\r");
  REprintf("Computing (%i percent), ETA: %.2f min.", perc, timetoend);
  
}
