#include "RcppArmadillo.h"

using namespace arma;
using namespace Rcpp;

// constants

const double log2pi = std::log(2.0 * M_PI);


// helper functions      ***********************************************************************************************

// these vectorized functions help speed up code when the main loop is run within R

//' Multivariate Normal Densities
//'
//' This function returns a vector of Multivariate Normal (log)-densities
//'
//' @usage dmvnrmam(x, mean, sigma, logd=FALSE)
//'
//' @param x matrix of observations
//' @param mean matrix of means matching the dimensions of x
//' @param sigma variance-covariance matrix
//'
//' @return A vector of (log)-densities
//'
//' @examples
//' dmvnrmam(matrix(c(.5,1,1,1),2,2,byrow=T), matrix(c(0.5,0.5),2,2), diag(2))
//'
//' @export
// [[Rcpp::export]]
arma::vec ldmvnrmam(arma::mat x,
                   arma::mat mean,
                   arma::mat sigma) {
  int n = x.n_rows;
  int xdim = x.n_cols;
  arma::vec out(n);
  arma::mat rooti = arma::trans(arma::inv(trimatu(arma::chol(sigma))));
  double rootisum = arma::sum(log(rooti.diag()));
  double constants = -(static_cast<double>(xdim)/2.0) * log2pi;
  
  for (int i=0; i < n; i++) {
    arma::vec z = rooti * arma::trans( x.row(i) - mean.row(i)) ;
    out(i)      = constants - 0.5 * arma::sum(z%z) + rootisum;
  }
  return(out);
}




//internal function for obtaining indices of order
uvec order_a(arma::vec x) {
  return(arma::stable_sort_index( x ) );
}





//' Multivariate Normal - Generate  Random Draws
//'
//' Generates MVN draws 
//' Relies on Armadillo, fast, no input checks
//'
//' @usage mvrnormArmavec(n, mu, sig)
//'
//' @param n number of draws
//' @param mu mean vector
//' @param sig Covariance Matrix
//' @return Vector; multivariate Normal draw
//'
//' @examples
//' mvrnorm(10, 1:3,diag(3)+.5)
//'
//' @export
// [[Rcpp::export]]
arma::mat mvrnorm(int n, arma::vec mu, arma::mat sigma) {
  int ncols = sigma.n_cols;
  arma::mat Y = arma::randn(n, ncols);
  return arma::repmat(mu, 1, n).t() + Y * arma::chol(sigma);
}





//' EVI random variates
//'
//'
//' @usage revd(n=100, loc=0, scale=1)
//'
//' @param n number of obs
//' @param loc location parameter
//' @param scale scale
//'
//' @return random variates
//'
//' @examples
//' revd(n=100, loc=0, scale=1)
//' @export
// [[Rcpp::export]]
vec revd(int n=100, double loc=0, double scale=1){
  return(loc-scale*log(-log(runif(n))));
}








// demand      ***********************************************************************************************



//' Optimal demand for Volumetric Demand Model (1 task/incidence)
//'
//' This function returns a vector of optimal demands given utilities \eqn{\psi}, satiation rate \eqn{\gamma}, budget constraint \eqn{E} and prices.
//' It is \eqn{\rm{D}} in the chapter.
//'
//' @usage vdm_demand(psi, gamma, E, prices)
//'
//' @param psi vector of utilities of all inside options in a choice set \eqn{\psi_j=a_j\beta+\varepsilon_j}
//' @param gamma numeric rate of satiation of onside good
//' @param E numeric budget constraint
//' @param prices vector of prices
//'
//' @return A vector of optimal demands
//'
//' @examples
//' vdm_demand(c(1,2),.9,10,c(2,3))
//'
//' @export
// [[Rcpp::export]]
vec vd_demand(arma::vec psi, double gamma, double E, vec prices) {
  arma::vec rho1 = prices / (psi);
  arma::uvec rho_ord=order_a(rho1);
  rho1 = rho1.elem(rho_ord);
  int J= prices.size();
  
  int rhos = rho1.size();
  arma::vec rho(rhos+2);
  rho(0) =0;
  rho(rhos+1)=R_PosInf;
  for (int i = 1; i <rhos+1; ++i){
    rho(i) = rho1(i-1);
  }
  
  arma::vec psi_ord =psi.elem(rho_ord);
  arma::vec prices_ord = prices.elem(rho_ord);
  double a = gamma*E;
  double b = gamma;
  double k = 0;
  double z = a/b;
  
  while( (z<=rho[k]) | (z>rho[k+1]) ){
    a=a+arma::as_scalar(prices_ord[k]);
    b=b+arma::as_scalar(psi_ord[k]);
    z=a/b;
    k=k+1;
  }
  arma::vec x = (psi_ord*z-prices_ord) /  (gamma*prices_ord);
  for (int i = k; i <J; ++i){
    x(i) = 0;
  }
  arma::vec X =x.elem(order_a(arma::conv_to<arma::vec>::from(rho_ord)));
  return(X);
}

double lndMvns(vec const& x, vec const& mu, mat const& sigma){
  
  arma::mat rooti = arma::trans(arma::inv(trimatu(arma::chol(sigma))));
  
  vec z = vectorise(trans(rooti)*(x-mu));
  
  return((-(x.size()/2.0)*log(2*M_PI) -.5*(trans(z)*z) + sum(log(diagvec(rooti))))[0]);
}
