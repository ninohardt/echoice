#include "RcppArmadillo.h"
using namespace arma;
using namespace Rcpp;

//borrowed from bayesm

//' Inverted Wishart distribution (internal function)
//'
//' This function returns a vector of Multivariate Normal (log)-densities
//'
//' @usage dmvnrmam(nu, V)
//'
//' @param nu matrix of observations
//' @param V matrix of means matching the dimensions of x
//'
//' @return List 
//'
//' @keywords internal 
//' @export
// [[Rcpp::export]]
List vrwishart(double nu, mat const& V){
  int m = V.n_rows;
  mat T = zeros(m,m);
  
  for(int i = 0; i < m; i++) {
    T(i,i) = sqrt(rchisq(1,nu-i)[0]); 
  }
  
  for(int j = 0; j < m; j++) {  
    for(int i = j+1; i < m; i++) {    
      T(i,j) = rnorm(1)[0]; 
    }}
  
  mat C = trans(T)*chol(V);
  mat CI = solve(trimatu(C),eye(m,m)); 
  
  return List::create(
    Named("W") = trans(C) * C,
    Named("IW") = CI * trans(CI),
    Named("C") = C,
    Named("CI") = CI);
}


//' Multivariate Regression (internal function)
//'
//' This function generates a draw from the posterior distribution of multivariate regression parameters
//'
//' @usage vrmultireg(Y, X, Bbar, A, nu, V)
//'
//' @return List 
//' @keywords internal 
//' @export
// [[Rcpp::export]]
List vrmultireg(mat const& Y, mat const& X, mat const& Bbar, mat const& A, double nu, mat const& V) {
  
  int n = Y.n_rows;
  int m = Y.n_cols;
  int k = X.n_cols;
  
  //first draw Sigma
  mat RA = chol(A);
  mat W = join_cols(X, RA); //analogous to rbind() in R
  mat Z = join_cols(Y, RA*Bbar);
  // note:  Y,X,A,Bbar must be matrices!
  mat IR = solve(trimatu(chol(trans(W)*W)), eye(k,k)); //trimatu interprets the matrix as upper triangular and makes solve more efficient
  // W'W = R'R  &  (W'W)^-1 = IRIR'  -- this is the UL decomp!
  mat Btilde = (IR*trans(IR)) * (trans(W)*Z);
  // IRIR'(W'Z) = (X'X+A)^-1(X'Y + ABbar)
  mat E = Z-W*Btilde;
  mat S = trans(E)*E;
  // E'E
  
  // compute the inverse of V+S
  mat ucholinv = solve(trimatu(chol(V+S)), eye(m,m));
  mat VSinv = ucholinv*trans(ucholinv);
  
  List rwout = vrwishart(nu+n, VSinv);
  mat CI = rwout["CI"];
  mat draw = mat(rnorm(k*m));
  draw.reshape(k,m);
  mat B = Btilde + IR*draw*trans(CI);
  
  return List::create(
    Named("B") = B, 
    Named("Sigma") = rwout["IW"]);
}


