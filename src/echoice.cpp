#include "RcppArmadillo.h"
#include "butil.h"
#include "mcmctimer.h"
#include "rmultireg_arma.h"
#include "vrmixGibbs.h"


// Profiler          ***********************************************************************************************

// profiler tools
// using gperf
//
// #include "gperftools/profiler.h"
// 
// using namespace Rcpp;
// 
// //' @export
// // [[Rcpp::export]]
// SEXP start_profiler(SEXP str) {
//   ProfilerStart(as<const char*>(str));
//   return R_NilValue;
// }
// //' @export
// // [[Rcpp::export]]
// SEXP stop_profiler() {
//   ProfilerStop();
//   return R_NilValue;
//}


// Helper          ***********************************************************************************************


// structs or lists can be used to pass draw information to main loop
struct vdmdraw{
  vec thetadraw;
  double stay;
  double oldll;
};


// intterupt checking, allows to return draws already generated upon user abort
  // interupt handling is dicussed here:
  // https://stackoverflow.com/questions/40563522/r-how-to-write-interruptible-c-function-and-recover-partial-results/40565675

static void chkIntFn(void *dummy) {
  R_CheckUserInterrupt();
}

// this will call the above in a top-level context so it won't longjmp-out of your context
bool checkInterrupt() {
  return (R_ToplevelExec(chkIntFn, NULL) == FALSE);
}


//auto-tuner to achieve target rejection rate window
void mh_tuner(vec tunes, vec rrs){
  int n = rrs.size();
  for(int ii=0; ii<n; ii++){
    //tuning
    if(rrs(ii)>0.8){
      tunes(ii)=tunes(ii)-tunes(ii)*.1;
    }else if(rrs(ii)<0.5){
      tunes(ii)=tunes(ii)+tunes(ii)*.1;
    }
    //guardrails
    if(tunes(ii)<0.001){
      tunes(ii)=0.01;
    }
    
    if(tunes(ii)>4){
      tunes(ii)=4;
    }
  }
}



// Standard VD model          ***********************************************************************************************


// quick overview of long format data
//  "ID"   "task"  "alternative"  "x"    "p"  "sxp"    n   "V1" 
//    1     2       3              4      5    6       7    8
//    0     1       2              3      4    5       6    7      


//' Volumetric Demand Model, individual-level log-likelihood
//'
//' This function returns the log-likehood for one individual, given her design matrix, prices and observed demand quantities stored in a long data format
//' This function assumes independent EV type I errors 
//' 
//' Vector of parameters theta is organized as follows: \eqn{\theta  = \left\{ {\beta ,\gamma ,E,\sigma } \right\}} or c(beta,gamma,E,sigma)
//'
//' @usage vdes_ll(voldatalongi, theta)
//'
//' @param damat data in voldatalong format
//' @param theta vector containing parameters
//' 
//' #' Format of  voldatalong:
//' It is a matrix with the following columns
//' \describe{
//' \item{ID}{Respondent or Customer ID}
//' \item{task}{Task number}
//' \item{alternative}{Choice alternative number}
//' \item{x}{Demand}
//' \item{p}{Price of a given alternative}
//' \item{sxp}{Total expenditure in given task (included for computational convenience)}
//' \item{n}{Number of choice alternative in given task}
//' \item{attributes}{Several columns containing the configuration of the corresponding alternative in attribute space}
//' }
//'
//' @return A numeric containing the log-likelihood
//'
//' @export
// [[Rcpp::export]]
double vdes_ll(mat const& damat, vec const& theta){

    //obtain parameters
  int p         = theta.size();
  vec beta      = theta(arma::span(0,p-4));
  double bud    = exp(theta(p-2));
  double gamma  = exp(theta(p-3));
  double sigma  = exp(theta(p-1));
  
  //task indicator vector
  arma::ivec task = conv_to<ivec>::from(damat.col(1));
  
  //inside solution indicator vector
  uvec xposf    = find(damat.col(3)>0);
  int npos      = xposf.size();
  ivec taskposf = task.elem(xposf);
  
  //split data into demand, price and attributes
  int ncdatam = damat.n_cols;
  mat A       = damat.cols(7,ncdatam-1);
  vec X       = damat.col(3);
  vec P       = damat.col(4);
  vec sxp     = damat.col(5);
  
  //mass/density contributions
  vec aBeta = sum(A.each_row() % trans(beta),1);
  vec gt    = -aBeta + log(gamma*X + 1) +log(P) - log(bud-sxp);
  const double out1 =  -sum(exp(-gt/sigma));
  const double out2 =   sum(-gt.elem(xposf)/sigma-log(sigma));
  
  //Jacobian
  double Jacob=0;
  if(npos>0){
    Jacob = sum((log(gamma) - log(gamma*X.elem(xposf)+1)) );
    vec jvec = ((gamma*X.elem(xposf)+1)%P.elem(xposf)) / (gamma*(bud-sxp.elem(xposf)));
    double jin=jvec(0);
    for(int jj=1; jj<npos; jj++){
      if(taskposf(jj-1)!=taskposf(jj)){
        Jacob+=log(jin+1);
        jin*=0;
      }
      jin+= jvec(jj);
    }
    Jacob+=log(jin+1);
  }
  
  return(Jacob+out1+out2);
}



//' Volumetric Demand Model, log-likelihood for N subjects
//'
//' This function returns the log-likehood for one individual, given her design matrix, prices and observed demand quantities stored in a long data format
//' This function assumes independent EV type I errors 
//' 
//' Vector of parameters theta is organized as follows: \eqn{\theta  = \left\{ {\beta ,\gamma ,E,\sigma } \right\}} or c(beta,gamma,E,sigma)
//'
//' @usage vdes_ll_N(voldatalong, vdmlookup, thetaM)
//' @keywords internal 
//' @param damat data in voldatalong format
//' @param vdmlookup lookup table for quick indexing
//' @param thetaM matrix containing parameters, individuals in rows, parameters in columns
//' 
//' #' Format of  voldatalong:
//' It is a matrix with the following columns
//' \describe{
//' \item{ID}{Respondent or Customer ID}
//' \item{task}{Task number}
//' \item{alternative}{Choice alternative number}
//' \item{x}{Demand}
//' \item{p}{Price of a given alternative}
//' \item{sxp}{Total expenditure in given task (included for computational convenience)}
//' \item{n}{Number of choice alternative in given task}
//' \item{attributes}{Several columns containing the configuration of the corresponding alternative in attribute space}
//' }
//'
//' @return A numeric vector containing log-likelihoods
//'
//' @export
// [[Rcpp::export]]
NumericVector vdes_ll_N(mat const& datain, umat const& vdmlookup, mat const& thetaM){
  int N = thetaM.n_rows;
  NumericVector out(N);
  
  for(int ij=0; ij<N; ij++){
    out(ij) = vdes_ll( datain.rows(vdmlookup(ij,1),vdmlookup(ij,2)), 
        trans(thetaM.row(ij)) );
  }
  return(out);
}


//' Volumetric Demand Model, log-likelihood for N subjects
//'
//' This function returns the log-likehood for one individual, given her design matrix, prices and observed demand quantities stored in a long data format
//' This function assumes independent EV type I errors 
//' 
//' Vector of parameters theta is organized as follows: \eqn{\theta  = \left\{ {\beta ,\gamma ,E,\sigma } \right\}} or c(beta,gamma,E,sigma)
//'
//' @usage vdes_ll_MLE(voldatalong, vdmlookup, theta, buds)
//' @keywords internal 
//' @param damat data in voldatalong format
//' @param vdmlookup lookup table for quick indexing
//' @param theta vector containing parameters
//' @param buds vector of budgets
//' 
//' #' Format of  voldatalong:
//' It is a matrix with the following columns
//' \describe{
//' \item{ID}{Respondent or Customer ID}
//' \item{task}{Task number}
//' \item{alternative}{Choice alternative number}
//' \item{x}{Demand}
//' \item{p}{Price of a given alternative}
//' \item{sxp}{Total expenditure in given task (included for computational convenience)}
//' \item{n}{Number of choice alternative in given task}
//' \item{attributes}{Several columns containing the configuration of the corresponding alternative in attribute space}
//' }
//'
//' @return A numeric containing the log-likelihood
//'
//' @export
// [[Rcpp::export]]
double vdes_ll_MLE(mat const& datain, 
                   umat const& vdmlookup, 
                          arma::vec theta,
                          arma::vec bud){
  
  int N = bud.size();
  mat parma= repmat(trans(theta),N,1);
  arma::mat thetaM = join_rows(join_rows(parma, log(bud)), zeros(N) );
  
  double out=0.0;
  for(int ij=0; ij<N; ij++){
    out+= vdes_ll( datain.rows(vdmlookup(ij,1),vdmlookup(ij,2)), 
        trans(thetaM.row(ij)) );
  }
  
  return(out);
}


  
  
  
  vdmdraw vdmevs2_drawone(mat const& voldatai,
                          vec theta,
                          double oldll,
                          double minbud,
                          double tune,
                          mat const& Gammabar,
                          mat const& SigmaTheta,
                          rowvec const& Zi){
    //one draw off posterior
    
    int p = theta.size();
    vdmdraw out_struct;
    
    mat ucholinv = solve(trimatu(chol(SigmaTheta)), eye(p,p));
    
    //candidate
    vec theta_new = theta + tune*trans(chol(ucholinv*trans(ucholinv)))*randn(p);
    
    //budget bound, reject if not permissible
    if(exp(theta_new(p-2))<=minbud){
      out_struct.oldll = oldll;
      out_struct.stay = 1;
      out_struct.thetadraw = theta;  
    }else{
      
      //MH
      //ll
      double ll_new = vdes_ll(voldatai,theta_new);
      //prior
      double pri     = lndMvns(theta,    trans(Gammabar)*trans(Zi),SigmaTheta);
      double pri_new = lndMvns(theta_new,trans(Gammabar)*trans(Zi),SigmaTheta);
      //MH
      double ldiff = ll_new + pri_new - oldll - pri;
      double alpha;
      if(ldiff==R_PosInf){
        alpha =1;
      }else{
        if(ldiff==R_NegInf){
          alpha =0;
        }else{
          alpha  = exp(ldiff);
        }
      }
      
      if(alpha>runif(1)(0)){
        out_struct.oldll = ll_new;
        out_struct.stay = 0;  
        out_struct.thetadraw = theta_new;
      }else{
        out_struct.oldll = oldll;
        out_struct.stay = 1;
        out_struct.thetadraw = theta;
      }
      
    }
    return (out_struct);
  }


  
//' Volumetric Demand Model, MCMC sampler (utility function)
//'
//' This function is called by rVDev, input checks are executed in rVDev.
//' @keywords internal 
//' @usage vdes_loop(voldatalong, vdmlookup, minbuds, Z, thetastart, p, N, R, keep, Bbar, A, nu, V, tuneinterval, steptunestart, tunelength, tunestart, progressinterval)
//'
//' @param voldatalong Data
//' @param vdmlookup lookup table
//' @param minbuds budget lower bound
//' @param Z Upper level covariates
//' @param thetastart start values
//' @param p number of parameters
//' @param N number of respondents
//' @param R number of draws
//' @param keep keep every keeps draws
//' @param Bbar Prior
//' @param A Prior
//' @param nu Prior
//' @param V Prior
//' @param tuneinterval How many draws to use for computing rejection rate
//' @param steptunestart initial tune parameters
//' @param tunelength how many draws for tuning
//' @param tunestart when to start tuning
//' @param progressinterval how often to update status in console
//' 
//' 
//' #' Format of  voldatalong:
//' It is a matrix with the following columns
//' \describe{
//' \item{ID}{Respondent or Customer ID}
//' \item{task}{Task number}
//' \item{alternative}{Choice alternative number}
//' \item{x}{Demand}
//' \item{p}{Price of a given alternative}
//' \item{sxp}{Total expenditure in given task (included for computational convenience)}
//' \item{n}{Number of choice alternative in given task}
//' \item{attributes}{Several columns containing the configuration of the corresponding alternative in attribute space}
//' }
//'
//' @return List of draws of theta, Gamma, Sigma_theta, logLike
//'
//' @export
// [[Rcpp::export]]
List vdes_loop( mat const& voldatalong, umat const& vdmlookup, vec const& minbuds, mat Z,
                   mat thetastart,
                   int p, int N,
                   int R, int keep,
                   mat const& Bbar, mat const& A, double nu, mat V,
                   int tuneinterval = 30, double steptunestart=.1, int tunelength=1000, int tunestart=500,
                   int progressinterval=100){

  // initialize temporary objects  ..................
    vdmdraw drawtemp;
    List rmreg;    
  
  // dimensions ..................
    int Rk=R/keep;
    int mkeep;
    int m=Z.n_cols;

  // start values ..................
    mat thetaM = thetastart;
    mat SigmaTheta = eye(p,p);  
    mat Gamma = Bbar;
    
  // tuning ..................
    mat stay(N,tuneinterval);
    int tunecounter = 0;
    vec tunes = ones<vec>(N)*steptunestart;
    vec rrs(N);  
    double currentRR=0;
    
  // initial log likelihood ..................
    vec oldlls = vdes_ll_N(voldatalong,vdmlookup,thetaM);

  // draw storage ..................
    cube thetaDraw(N,p,Rk);
    cube SigmaThetaDraw(p,p,Rk);
    
    vec  logLike = zeros<vec>(Rk);
    mat  GammaDraw(Rk,p*m);  
    
  // loop ..................    
    startMcmcTimer();
    for(int ir=0; ir<R; ir++){
    Rcpp::checkUserInterrupt();

      // upper level *********
       rmreg =  vrmultireg( thetaM, Z,  Bbar,  A, nu, V);
        SigmaTheta = as<mat>(rmreg["Sigma"]);
        Gamma      = as<mat>(rmreg["B"]);
  
      
      // n loop  *********
        for(int ij=0; ij<N; ij++){
          drawtemp =  vdmevs2_drawone( voldatalong.rows(vdmlookup(ij,1),vdmlookup(ij,2)),
                                       trans(thetaM.row(ij)),
                                       oldlls(ij),
                                       minbuds(ij),
                                       tunes(ij),
                                       Gamma,
                                       SigmaTheta,
                                       Z.row(ij));
          //update
          oldlls(ij)    = drawtemp.oldll;
          thetaM.row(ij)= trans(drawtemp.thetadraw);
          
          //tune data
          stay(ij,tunecounter) = drawtemp.stay;
        }

  // tuning stuff ..................
    tunecounter+=1;
    if(tunecounter>=tuneinterval){
      //rejection rate in window
      rrs=mean(stay,1);
      tunecounter=0;
      stay.fill(0);
      
      //just for progress output
      currentRR=mean(rrs); 
      //tune within tune range
      if( (ir>=tunestart) & (ir<(tunestart+tunelength))){
        mh_tuner(tunes,rrs);
      }
    }
    // end of loop

    
  // save draws  ..................
    if((ir+1)%keep==0){
      mkeep = (ir+1)/keep-1;
      thetaDraw.slice(mkeep)      = thetaM;
      GammaDraw.row(mkeep)        = trans(vectorise(Gamma,0));
      SigmaThetaDraw.slice(mkeep) = SigmaTheta;
      logLike(mkeep)              = sum(oldlls);
    }
    
  // display progress  ..................
    if((ir+1)%progressinterval==0){
      infoMcmcTimerRRLL(ir,R, currentRR, sum(oldlls));
    }

  }
  endMcmcTimer();
  //return draws  ..................
    return List::create(
      Named("thetaDraw")      = thetaDraw,
      Named("GammaDraw")      = GammaDraw,
      Named("SigmaThetaDraw") = SigmaThetaDraw,
      Named("logLike")        = logLike);
    
}





// Demand predictions          ***********************************************************************************************


// quick overview of long format data
//  "ID"   "task"  "alternative"  "x"    "p"  "sxp"    n   draw   "V1" 
//    1     2       3              4      5    6       7    8       9
//    0     1       2              3      4    5       6    7       8

// Lookup type B
//          ID task  from    to uid
//   [1,]    7    1     1     8   1





//' @export
//' @keywords internal 
//[[Rcpp::export]]
Rcpp::DataFrame vdes_demand_rcpp_long(    cube thetaDraw, 
                                          NumericMatrix datain, 
                                          umat vdmlookupB,
                                          int neps=1) {
  
  //obtain data (design matrices)
  arma::mat damat(datain.begin(), datain.rows(), datain.cols(), false, true);
 // Rcout << "xx";
  //dimensions
  //int nunits    = thetaDraw.n_rows;
  int p         = thetaDraw.n_cols;
  int ndraws    = thetaDraw.n_slices; 
  int nps       = vdmlookupB.n_rows;  // IDxtask combinations
  int nx        = damat.n_rows;       // IDxtaskxalt
  int outputdim = nx*ndraws;
  int ncdatam   = damat.n_cols;
  
  //initialize output objects
  vec output(outputdim); //doubles for demand
  ivec outID(outputdim); //integers for rest
  ivec taskID(outputdim);
  ivec altID(outputdim);
  ivec drawID(outputdim);
  
  //define parameters
  vec beta(p-3);
  cube betat(1,p-3,1);
  
  double gamma;
  double bud;
  double sigma;
  //double delta;
  int nalt;
  
  //unique counter
  int kk=0;
  startTimer();

    //loop over IDxtask combinations
  for (int ij = 0; ij <nps; ++ij){
    Rcpp::checkUserInterrupt();
    infoTimer(ij,nps);
    
    //vdmlookupB(ij,2),vdmlookupB(ij,3)
    for (int ir = 0; ir <ndraws; ++ir){

      //choice parameters
      betat   = thetaDraw(span(vdmlookupB(ij,4)), arma::span(0,p-4), span(ir) );
      arma::vec beta(betat.begin(), p-3,  false, true);
      gamma  = exp(thetaDraw(vdmlookupB(ij,4), p-3, ir ));
      bud    = exp(thetaDraw(vdmlookupB(ij,4), p-2, ir ));
      sigma  = exp(thetaDraw(vdmlookupB(ij,4), p-1, ir ));
      //delta  = exp(thetaDraw(vdmlookupB(ij,4), p-1, ir ));

      //number of alternatives
      nalt = (vdmlookupB(ij,3)-vdmlookupB(ij,2)+1);
      
      //genereate elements for output data.frame
      
      //demand conditional on alternatives, theta, eps
      
      //adding eps realizations ----------
      if(neps>1){
        vec temp_dem(nalt);
        temp_dem.fill(0);
        for (int ire = 0; ire <neps; ++ire){
          temp_dem+=
            vd_demand( exp(damat.submat(vdmlookupB(ij,2) ,1 ,vdmlookupB(ij,3) ,ncdatam-1) * beta  + revd(nalt, 0, sigma) ),
                       gamma, bud, 
                       damat.submat(vdmlookupB(ij,2) ,0 ,vdmlookupB(ij,3) ,0));
        }
        output(arma::span(kk,kk+nalt-1))=temp_dem/neps;
      } else{
        //one eps per theta draw
        output(arma::span(kk,kk+nalt-1))=
          vd_demand( exp(damat.submat(vdmlookupB(ij,2) ,1 ,vdmlookupB(ij,3) ,ncdatam-1) * beta  + revd(nalt, 0, sigma) ),
                     gamma, bud, 
                     damat.submat(vdmlookupB(ij,2) ,0 ,vdmlookupB(ij,3) ,0));
      }

      //other d.f elements
      outID(arma::span(kk,kk+nalt-1)).fill(vdmlookupB(ij,0));     //ID
      taskID(arma::span(kk,kk+nalt-1)).fill(vdmlookupB(ij,1));    //task  
      altID(span(kk,kk+nalt-1))=linspace<ivec>(1, nalt, nalt);    //alt
      drawID(arma::span(kk,kk+nalt-1)).fill(ir);                  //draw
      
      kk+=nalt; //update counter
    }
    
  }
  REprintf("\n Computation complete, returning completed data.frame \n");
  
  return(Rcpp::DataFrame::create( Named("ID")         = outID, 
                                  Named("task")       = taskID,
                                  Named("alternative")= altID,
                                  Named("draw")       = drawID,
                                  Named("x")          = output));
  
}

