---
title: "echoice"
output: github_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```


# ![echoice 0.3](man/figures/echoicelogo.png)

This package contains choice models with economic foundation.
In its current version, it implements a discrete-continous choice model, including an MCMC estimation algorithm and functions for predicting demand.

For more theoretical background, please refer to the chapter 'economic foundations of conjoint' in the follwing handbook:
https://www.elsevier.com/books/handbook-of-the-economics-of-marketing/dube/978-0-444-63759-8

For details on how to use echoice, please refer to the vignette in the package. It illustrates model estimation, predictions and model evaluation.


### Version 0.3

echoice 0.3 is a rewrite of the earlier 0.2 version. 

* The complete algorithm is implemented in cpp, yielding faster estimation
* It works well with tidy syntax, which is illustrated in the vignette


The old version can be found here: https://bitbucket.org/ninohardt/echoice02



### Installation

This package has not been published on CRAN yet. You can install it using the remotes package.

```{r cars, eval=FALSE, include=TRUE}
remotes::install_bitbucket("ninohardt/echoice")
```


### Using echoice

Please read the vignette. It illustrates a complete workflow from estimation to market simulation.

